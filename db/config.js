const { drawDetections } = require("face-api.js/build/commonjs/draw");
const mongoose = require("mongoose")


var db = mongoose.createConnection(process.env.MONGO, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
})




module.exports = db;