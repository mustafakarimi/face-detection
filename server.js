const express = require('express');
const app = express();
const multer = require("multer");
require("dotenv").config()
require("./db/config")
const path = require("path")
const { loginUser } = require("./api/login/login")
const { registerUser } = require("./api/register/register")
const faceApi = require("face-api.js");
const { v4 } = require('uuid');
const tf = require("@tensorflow/tfjs-node")

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, v4() + ".jpg");
    }
  })

const upload = multer({ dest: 'uploads/', storage: storage })




// env().set('WEBGL_CPU_FORWARD', false)

app.use(express.urlencoded({extended: false}))

faceApi.nets.faceRecognitionNet.loadFromDisk("./face_models/");
faceApi.nets.ssdMobilenetv1.loadFromDisk("./face_models/");
faceApi.nets.faceLandmark68Net.loadFromDisk("./face_models/");

app.post("/login", upload.single("image"), loginUser)
app.post("/register", upload.array("images[]", 10), registerUser)


app.listen(process.env.PORT | 8000, () => {
    console.log("Server is up and running...")
})


