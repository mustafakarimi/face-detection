const { setBackend, mod } = require("@tensorflow/tfjs-core");
const { Schema } = require("mongoose");
const { v4 } = require("uuid");
const db = require("../db/config");

var schema = new Schema({
    id: {type: String, default: v4()},
    name: { type: String, default: 'username' },
    email: { type: String},
    bio: { type: String, match: /[a-z]/ },
    date: { type: Date, default: Date.now },
    images: { type: Array },
})
var model = db.model("User", schema);

module.exports = model;