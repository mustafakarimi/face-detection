const userModel = require("../../models/user")
const fs = require("fs");
const path = require("path");
const { v4 } = require("uuid")
const faceApi = require("face-api.js");
const { Canvas , Image, loadImage} = require('canvas');
const fetch = require('node-fetch');
const { env } = require("@tensorflow/tfjs-core");
const { cwd } = require("process");

faceApi.env.monkeyPatch({ Canvas, Image, fetch: fetch})


module.exports = {
    async registerUser(req,res) {

        var email = req.body.email;
        var name = req.body.name;
        var bio = req.body.bio;
        var files = req.files;


        var filePaths = req.files.map((image) => image.path);


        var uuid = v4();
        await userModel.create({
            id: uuid,
            email,
            name,
            bio,
            date: new Date(),
            images: filePaths
        })

        var isExists = fs.existsSync(path.join(cwd(), "uploads", name))
        if (!isExists) {
            fs.mkdir(path.join(cwd(),"uploads", name), (err) => {
                console.log("Path Created")
            })
        }



        for (file of files) {
            await fs.createReadStream(file.path).pipe(fs.createWriteStream(path.join(cwd(),"uploads",name, file.filename + ".jpg")));
        }

        res.status(200).json({
            state: "success",
            code: 200,
            message: "user registered"
        })
    }
}