const faceApi = require("face-api.js");
const fs = require("fs");
const { cwd } = require("process");
const path = require("path");
const { loadImage } = require("canvas");

module.exports = {
    async loginUser(req,res) {
        var uploadedFile = req.file;
        var loginImage = uploadedFile;
        var loadedImage = await loadImage(path.join(cwd(),loginImage.path));
        var detect = await faceApi.detectSingleFace(loadedImage).withFaceLandmarks().withFaceDescriptor()

        fs.readdir(path.join(cwd(), "uploads"), async (err, files) => {
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            } 
            for (file of files) {
                console.log(file)
                var _path = path.join(cwd() , "uploads", file);
                fs.lstat(_path,async (err, types) => {
                    if (err) {
                        return console.log(err);
                    } 
                    if (types.isFile()) {
                        var image  = await loadImage(path.join(cwd(), "uploads",file));
                        var faces = await faceApi.detectSingleFace(image).withFaceLandmarks().withFaceDescriptor();
                        var faceMatcher = new faceApi.FaceMatcher(faces);
                        var match = await faceMatcher.findBestMatch(detect.descriptor)
                        console.log(match.toString());
                    }
                })
            }
        });
        res.status(200).json({done: "done"})
        
    }
}